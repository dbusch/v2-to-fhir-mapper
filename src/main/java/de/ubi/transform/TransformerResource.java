/*
 * Copyright (c) 2022. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 * Morbi non lorem porttitor neque feugiat blandit. Ut vitae ipsum eget quam lacinia accumsan.
 * Etiam sed turpis ac ipsum condimentum fringilla. Maecenas magna.
 * Proin dapibus sapien vel ante. Aliquam erat volutpat. Pellentesque sagittis ligula eget metus.
 * Vestibulum commodo. Ut rhoncus gravida arcu.
 */
package de.ubi.transform;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.parser.IParser;
import ca.uhn.fhir.rest.api.MethodOutcome;
import ca.uhn.fhir.rest.client.api.IGenericClient;
import io.github.linuxforhealth.hl7.ConverterOptions;
import io.github.linuxforhealth.hl7.HL7ToFHIRConverter;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.Context;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import org.hl7.fhir.r4.model.Bundle;
import java.util.Map;

@Path("api")
public class TransformerResource {
    public static HL7ToFHIRConverter V2TOFHIR_CONV;
    public static ConverterOptions OPTIONS;

    public static IParser PARSER;
    private static FhirContext FCTX;

    @POST
    @Path("transform")
    @Produces(MediaType.APPLICATION_JSON)
    public Response transform(@Context HttpServletRequest request) {
        init();
        try {
            Map<String, String> body = Helper.readRawBody(request);
            if (body.containsKey(Helper.BODY_RESULT)) {
                Map<String, Object> map = Helper.objectMapper()
                        .readValue(body.get(Helper.BODY_RESULT), Map.class);
                if (map.containsKey("content")) {
                    Bundle b = V2TOFHIR_CONV.convertToBundle(map.get("content").toString(), OPTIONS, null);
                    return Response.ok(PARSER.encodeResourceToString(b)).build();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
    }

    @POST
    @Path("sendTo")
    @Produces(MediaType.APPLICATION_JSON)
    public Response sendTo(@Context HttpServletRequest request, @HeaderParam("destination") String destination) {
        init();
        try {
            Map<String, String> body = Helper.readRawBody(request);
            if (body.containsKey(Helper.BODY_RESULT) && destination != null && !destination.isEmpty()) {

                IGenericClient client = getClient(destination);
                Bundle bundle = Helper.getR4JsonParser().parseResource(Bundle.class, body.get(Helper.BODY_RESULT));
                if (bundle != null && !bundle.getEntry().isEmpty()) {
                    MethodOutcome outcome = client.create()
                            .resource(bundle)
                            .prettyPrint()
                            .encodedJson()
                            .execute();
                    if (outcome.getResponseHeaders().containsKey("content-location"))
                        return Response.ok(outcome.getResponseHeaders().get("content-location").iterator().next()).build();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
    }

    @POST
    @Path("transformAndSend")
    @Produces(MediaType.APPLICATION_JSON)
    public Response transformAndSend(@Context HttpServletRequest request, @HeaderParam("destination") String destination) {

        init();
        try {
            Map<String, String> body = Helper.readRawBody(request);
            if (body.containsKey(Helper.BODY_RESULT) && destination != null && !destination.isEmpty()) {

                Map<String, Object> map = Helper.objectMapper()
                        .readValue(body.get(Helper.BODY_RESULT), Map.class);
                if (map.containsKey("content")) {
                    Bundle bundle = V2TOFHIR_CONV.convertToBundle(map.get("content").toString(), OPTIONS, null);


                    IGenericClient client = getClient(destination);
                    if (bundle != null && !bundle.getEntry().isEmpty()) {
                        MethodOutcome outcome = client.create()
                                .resource(bundle)
                                .prettyPrint()
                                .encodedJson()
                                .execute();
                        if (outcome.getResponseHeaders().containsKey("content-location"))
                            return Response.ok(outcome.getResponseHeaders().get("content-location").iterator().next()).build();

                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
    }

    private void init() {
        if (V2TOFHIR_CONV == null)
            V2TOFHIR_CONV = new HL7ToFHIRConverter();

        if (OPTIONS == null)
            OPTIONS = new ConverterOptions.Builder().withValidateResource().withPrettyPrint().build();

        if (PARSER == null)
            PARSER = FhirContext.forR4().newJsonParser().setPrettyPrint(true);
    }

    private IGenericClient getClient(String destination) {
        if (FCTX == null) {
            FCTX = FhirContext.forR4();
        }
        return FCTX.newRestfulGenericClient(destination);
    }
}
