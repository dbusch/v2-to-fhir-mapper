package de.ubi.transform;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.parser.IParser;
import com.fasterxml.jackson.core.json.JsonReadFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.json.JsonMapper;
import jakarta.servlet.http.HttpServletRequest;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

public class Helper {

    public static final String BODY_RESULT = "body";
    public static final String ERROR_RESULT = "error";

    //private static Gson gson;
    private static ObjectMapper objectMapper;
    private static IParser PARSER;

    /**
     * @param request
     * @return Map mit body oder error als Key
     */
    public static Map<String, String> readRawBody(HttpServletRequest request) {
        Map<String, String> result = new HashMap<>();
        StringBuilder stringBuilder = new StringBuilder();
        try {
            InputStream inputStream = request.getInputStream();
            if (inputStream != null) {
                try (BufferedReader bufferedReader = new BufferedReader(
                        new InputStreamReader(inputStream, StandardCharsets.UTF_8))) {
                    char[] charBuffer = new char[128];
                    int bytesRead;
                    while ((bytesRead = bufferedReader.read(charBuffer)) > 0) {
                        stringBuilder.append(charBuffer, 0, bytesRead);
                    }
                }
            } else {
                throw new IllegalStateException("Fehler InputStream ist NULL, versuche BufferedReader");
            }
            result.put(BODY_RESULT, stringBuilder.toString());
            return result;
        } catch (IllegalStateException e) {
            try {
                BufferedReader reader = request.getReader();
                String line;
                while ((line = reader.readLine()) != null) {
                    stringBuilder.append(line);
                }
                result.put(BODY_RESULT, stringBuilder.toString());
                return result;
            } catch (Exception ex) {
                result.put(ERROR_RESULT, ex.getMessage());
                e.printStackTrace();
            }
        } catch (Exception e) {
            result.put(ERROR_RESULT, e.getMessage());
            e.printStackTrace();
        }
        return result;
    }


    public static ObjectMapper objectMapper() {
        if (objectMapper == null) {
            objectMapper = JsonMapper.builder()
                    .enable(JsonReadFeature.ALLOW_BACKSLASH_ESCAPING_ANY_CHARACTER)
                    .build();
        }
        return objectMapper;
    }
    public static IParser getR4JsonParser() {
        if(PARSER == null){
            PARSER = FhirContext.forR4().newJsonParser().setPrettyPrint(true);
        }
        return PARSER;
    }

  /*  public static Gson gson() {
        if (gson == null) {
            gson = new Gson();
        }
        return gson;
    }
 */
}

